# React Native App
A React Native application for showing the list of medicines

## Installation
1. Clone the repository
 
  git clone https://github.com/your-username/site-info-react-native.git    ```

2. Navigate to the project directory
    cd project_name    ```

3. Install dependencies
    ```bash    npm install
    or yarn install
## Step 1: Start the Metro Server
First, you will need to start **Metro**, the JavaScript _bundler_ that ships _with_ React Native.
To start Metro, run the following command from the _root_ of your React Native project:
```bash# using npm
npm start
# OR using Yarnyarn start```
## Step 2: Start your Application### For Android
```bash# using npm
npx react-native run-android
# OR using Yarnyarn run android
