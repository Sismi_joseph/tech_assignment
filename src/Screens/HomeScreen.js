import React, { useEffect, useState } from 'react';
import {
    View,
    Text,
    StyleSheet,
    SafeAreaView,
    StatusBar,
    TouchableOpacity,
    FlatList,
    Image,
    ActivityIndicator
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import moment from 'moment';
import axios from 'axios';
import Icon from 'react-native-vector-icons/AntDesign';
import DatePicker from 'react-native-date-picker';
import { BACKGROUNDCOLOR, DUMMYIMAGE, PRIMARYCOLOR, TEXTCOLOR } from '../Constants/colors';
import { windowHeight, windowWidth } from '../Constants/Styles';
import { baseUrl } from '../Constants/baseUrl';

const HomeScreen = () => {

    const [date, setDate] = useState(new Date());
    const [open, setOpen] = useState(false);
    const [MedData, setMedData] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    const [token, setToken] = useState(null);

    useEffect(() => { //FUNCTION FOR FETCHING THE ASYNC VALUE
        const fetchData = async () => {
            const storedToken = await _retrieveData();
            if (storedToken) {
                setToken(storedToken);
            }
        };

        fetchData();
    }, []);


    const _retrieveData = async () => {
        try {
            const value = await AsyncStorage.getItem('token');
            if (value !== null) {
                setIsLoading(true);
                return value;
            }
        } catch (error) {
            console.log(error);
            // Handle error retrieving data
        }
        return null;
    };

    useEffect(() => {
        if (token) {
            getMedicines()
        }
    }, [token, date])


    const getMedicines = async () => { //FUNCTION FOR CALLING THE API
        setIsLoading(true);

        const req = {
            selected_date: moment(date).format('YYYY/MM/DD')
        };

        const header = {
            'Authorization': `Bearer ${token}`,
            'Content-Type': 'application/json'
        };

        try {
            const response = await axios.post(baseUrl + 'dashboard', req, { headers: header });
            if(response?.data?.data?.medicines_for_today?.length>0){
                setMedData(response?.data?.data);
            }
    

        } catch (error) {
            console.log(error);
        } finally {
            setIsLoading(false);
        }
    };

    const renderItem = ({ item }) => {
        return (
            <View style={styles.card}>
                <View style={styles.imgView}>
                    <Image
                        resizeMode='contain'
                        source={{ uri: DUMMYIMAGE }}
                        style={styles.img}
                    />
                </View>

                <View style={{ paddingLeft: windowWidth * 0.05 }}>
                    <Text style={styles.textName}>{item?.medicine_name} ({item?.medicine_strength})</Text>
                    <Text style={styles.textName}>Frequency: <Text style={styles.textDate}>{item?.frequency?.frequency}</Text></Text>

                    <Text style={styles.textName}>Scheduled Time:</Text>
                    {item?.usermedicineshedule?.schedule_time?.map((time, index) => (
                        <Text key={index} style={styles.textDate}>{time?.time_slot}</Text>
                    ))}
                </View>
            </View>
        );
    };

    return (
        <SafeAreaView style={styles.container}>
            <StatusBar barStyle='dark-content' backgroundColor={BACKGROUNDCOLOR} />

            <View style={styles.row}>
                <View>
                    <Text style={styles.textDate}>{moment(date).format('YYYY/MM/DD')}</Text>
                </View>
                <View>
                    <TouchableOpacity onPress={() => setOpen(true)}>
                        <Icon name='calendar' color={PRIMARYCOLOR} size={25} />
                    </TouchableOpacity>
                </View>
            </View>

            {isLoading ? (
                <View style={styles.loadContainer}>
                    <ActivityIndicator size='large' color={PRIMARYCOLOR} style={{ marginTop: 20 }} />
                </View>

            ) : (
                <View>
                    {MedData?.medicines_for_today?.length > 0 ?
                        <FlatList
                            data={MedData?.medicines_for_today || []}
                            keyExtractor={(item) => item?.id.toString()}
                            renderItem={renderItem}
                            contentContainerStyle={{ paddingBottom: 20 }}
                        /> :
                        <View style={styles.loadContainer}>
                            <Text style={styles.textDate}>No Data Found</Text>
                        </View>}
                </View>
            )}

            <DatePicker //DATE PICKER FOR SELECTING THE DATE
                modal
                open={open}
                date={date}
                onConfirm={(date) => {
                    setOpen(false);
                    setDate(date);
                    setMedData([])
                    getMedicines()
                }}
                onCancel={() => setOpen(false)}
            />
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: BACKGROUNDCOLOR
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderWidth: 1,
        borderColor: TEXTCOLOR,
        width: windowWidth * 0.8,
        height: windowWidth * 0.13,
        borderRadius: 10,
        alignItems: 'center',
        padding: 10,
        margin: windowWidth * 0.05
    },
    textDate: {
        color: TEXTCOLOR,
        fontSize: 14
    },
    img: {
        width: windowWidth * 0.7,
        height: windowHeight * 0.2,
    },
    card: {
        elevation: 5,
        width: windowWidth * 0.8,
        backgroundColor: BACKGROUNDCOLOR,
        borderRadius: 10,
        padding: 15,
        margin: 10
    },
    textName: {
        color: TEXTCOLOR,
        fontSize: 16,
        fontWeight: '700'
    },
    imgView: {
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10
    },
    loadContainer: {
        flex: 0.9,
        alignItems: 'center',
        justifyContent: 'center'
    }
});

export default HomeScreen;
