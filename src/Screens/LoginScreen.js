
import React, { useState } from 'react';
import {
    View,
    Text,
    StyleSheet,
    SafeAreaView,
    StatusBar,
    TextInput,
    TouchableOpacity,
    ActivityIndicator,
    ToastAndroid,
    Keyboard
} from 'react-native';
import { BACKGROUNDCOLOR, PRIMARYCOLOR, TEXTCOLOR } from '../Constants/colors';
import { windowHeight, windowWidth } from '../Constants/Styles';
import axios from 'axios';
import { baseUrl } from '../Constants/baseUrl';
import AsyncStorage from '@react-native-async-storage/async-storage';

const LoginScreen = ({ navigation }) => {

    const [Email, setEmail] = useState(null);
    const [Password, setPassword] = useState(null);
    const [isLoading, setIsLoading] = useState(false)

    const OnLogin = () => { //FUNCTION FOR LOGIN THE USER

        setIsLoading(true)

        Keyboard.dismiss()

        axios.post(baseUrl + 'login', {
            "email": Email,
            "password": Password,
            "fcm_token": "xxxxxxxxxxxxx",
            "zone": "Asia/Kolkata"
        })
            .then(function (response) {
              
                setIsLoading(false)
                
                showToast(response?.data?.message)
            
                AsyncStorage.setItem('token',response?.data?.data?.token)

                setTimeout(() => {
                    navigation.navigate('Home')
                }, 2000);
                
            })
            .catch(function (error) {
                setIsLoading(false)
                showToast(error?.response?.data?.message)
            });
    }

    const showToast = (message) => { //FUNCTION FOR SHOWING THE TOAST MESSAGES
        ToastAndroid.show(message, ToastAndroid.SHORT);
    };


    return (
        <SafeAreaView style={styles.container}>
            <StatusBar barStyle={'dark-content'} backgroundColor={BACKGROUNDCOLOR} />

            <View>
                <Text style={styles.textLogin}>Login</Text>
            </View>

            <View>

                <View>
                    <Text style={styles.textTitle}>Username</Text>
                </View>

                <View>
                    <TextInput
                        style={styles.input}
                        value={Email}
                        onChangeText={(text) => setEmail(text)} />
                </View>
            </View>

            <View>

                <View>
                    <Text style={styles.textTitle}>Password</Text>
                </View>

                <View>
                    <TextInput
                        style={styles.input}
                        value={Password}
                        onChangeText={(text) => setPassword(text)} />
                </View>
            </View>

            <View>
                <TouchableOpacity style={styles.btn} onPress={() => OnLogin()}>
                    <Text style={styles.textBtn}>Login</Text>
                    {isLoading &&
                        <ActivityIndicator size={20} color={BACKGROUNDCOLOR} />}
                </TouchableOpacity>
            </View>

        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: BACKGROUNDCOLOR,
    },
    textLogin: {
        color: TEXTCOLOR,
        fontSize: 25,
        fontWeight: '700',
        marginBottom:20
    },
    textTitle: {
        color: TEXTCOLOR,
        fontSize: 14,
        marginBottom:10
    },
    input: {
        color: TEXTCOLOR,
        fontSize: 14,
        borderWidth: 1,
        borderColor: TEXTCOLOR,
        width: windowWidth * 0.80,
        borderRadius: 10,
        marginBottom: windowHeight * 0.03,
        paddingLeft: 10
    },
    textBtn: {
        color: BACKGROUNDCOLOR,
        fontSize: 14,
        fontWeight: '600',
        paddingRight: 10
    },
    btn: {
        backgroundColor: PRIMARYCOLOR,
        width: windowWidth * 0.80,
        height: windowWidth * 0.14,
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row'
    }

});

export default LoginScreen;